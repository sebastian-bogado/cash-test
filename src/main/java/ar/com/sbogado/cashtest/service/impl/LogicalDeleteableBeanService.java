package ar.com.sbogado.cashtest.service.impl;

import ar.com.sbogado.cashtest.model.LogicalDeleteableBean;

public abstract class LogicalDeleteableBeanService<T extends LogicalDeleteableBean> extends AbstractBaseBeanService<T> {

	@Override
	protected T prepareToCreate(T baseBean) {
		baseBean.setActive(true);
		return super.prepareToCreate(baseBean);
	}

}
