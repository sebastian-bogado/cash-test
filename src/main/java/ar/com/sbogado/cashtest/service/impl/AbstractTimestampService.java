package ar.com.sbogado.cashtest.service.impl;

import ar.com.sbogado.cashtest.model.TimestampBean;

import java.util.Date;

public class AbstractTimestampService<T extends TimestampBean> {

	protected T prepareToCreate(T baseBean) {
		baseBean.setCreatedFrom(new Date());
		baseBean.setLastUpdate(new Date());
		return baseBean;
	}

	protected T prepareToUpdate(T baseBean) {
		baseBean.setLastUpdate(new Date());
		return baseBean;
	}

}
