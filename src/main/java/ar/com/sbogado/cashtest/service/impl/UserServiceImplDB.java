package ar.com.sbogado.cashtest.service.impl;

import ar.com.sbogado.cashtest.dao.UserDao;
import ar.com.sbogado.cashtest.exception.UserExistException;
import ar.com.sbogado.cashtest.exception.UserNotFoundException;
import ar.com.sbogado.cashtest.model.User;
import ar.com.sbogado.cashtest.service.UserService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImplDB extends LogicalDeleteableBeanService<User> implements UserService {
	@Autowired
	private UserDao userDao;

	@Override
	public List<User> readActiveUsers() {
		return userDao.findAllByActiveIsTrue();
	}

	@Override
	public Page<User> readActiveUsers(Pageable pageable) {
		return userDao.findAllByActiveIsTrue(pageable);
	}

	@Override
	public Optional<User> readUserById(Long id) {
		return userDao.findById(id);
	}

	@Override
	public User readUserWithAllInfoById(Long id) {
		User user = userDao.findByIdAndActiveIsTrue(id).orElseThrow(() -> new UserNotFoundException(id));
		Hibernate.initialize(user);
		return user;
	}

	@Override
	public User createUser(User user) {
		Optional<User> optUser = userDao.findByEmail(user.getEmail());
		if(optUser.isPresent()) {
			throw new UserExistException(user.getEmail());
		}
		return userDao.save(prepareToCreate(user));
	}

	@Override
	public User updateUser(@NotNull User user) {
		Optional<User> optUser = userDao.findByEmail(user.getEmail());
		if(optUser.isPresent() && !optUser.get().getId().equals(user.getId())) {
			throw new UserExistException(user.getEmail());
		}
		User persistedUser = userDao.findById(user.getId()).orElseThrow(() -> new UserNotFoundException(user.getId()));
		persistedUser.setEmail(user.getEmail());
		persistedUser.setFirstName(user.getFirstName());
		persistedUser.setLastName(user.getLastName());
		return userDao.save(prepareToUpdate(persistedUser));
	}

	@Override
	public void deleteUser(User user) {
		userDao.delete(user);
	}

	@Override
	public void deleteUser(Long id) {
		userDao.deleteById(id);
	}
}
