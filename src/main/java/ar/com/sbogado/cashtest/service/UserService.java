package ar.com.sbogado.cashtest.service;

import ar.com.sbogado.cashtest.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {

	List<User> readActiveUsers();
	Page<User> readActiveUsers(Pageable pageable);
	Optional<User> readUserById(Long id);
	User readUserWithAllInfoById(Long id);
	User createUser(User user);
	User updateUser(User user);
	void deleteUser(User user);
	void deleteUser(Long id);

}
