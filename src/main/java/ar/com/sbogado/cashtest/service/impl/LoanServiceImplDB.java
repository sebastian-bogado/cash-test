package ar.com.sbogado.cashtest.service.impl;

import ar.com.sbogado.cashtest.dao.LoanDao;
import ar.com.sbogado.cashtest.dao.filter.LoanFilter;
import ar.com.sbogado.cashtest.exception.LoanNotFoundException;
import ar.com.sbogado.cashtest.exception.NotValidFormatException;
import ar.com.sbogado.cashtest.model.Loan;
import ar.com.sbogado.cashtest.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class LoanServiceImplDB extends LogicalDeleteableBeanService<Loan> implements LoanService {
	@Autowired
	private LoanDao loanDao;

	@Override
	public Page<Loan> findByFilter(LoanFilter filter, Pageable pageable) {
		return loanDao.findLoansByFilterAndActiveIsTrue(filter, pageable);
	}

	@Override
	public Loan createLoan(Loan loan) {
		return loanDao.save(prepareToCreate(loan));
	}

	@Override
	public Loan updateLoan(Loan loan) {
		if(loan.getId() == null) {
			throw new NotValidFormatException("invalid.format.exception.loan.has.not.id");
		}
		Loan persistedLoan = loanDao.findById(loan.getId()).orElseThrow(() -> new LoanNotFoundException("loan.not.found.exception.description.update"));
		persistedLoan.setTotal(loan.getTotal());
		return prepareToUpdate(persistedLoan);
	}

	@Override
	public void deleteLoan(Loan loan) {
		loanDao.delete(loan);
	}

	@Override
	public void deleteLoan(Long id) {
		loanDao.deleteById(id);
	}
}
