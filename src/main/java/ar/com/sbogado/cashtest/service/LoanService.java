package ar.com.sbogado.cashtest.service;

import ar.com.sbogado.cashtest.dao.filter.LoanFilter;
import ar.com.sbogado.cashtest.model.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LoanService {

	Page<Loan> findByFilter(LoanFilter filter, Pageable pageable);
	Loan createLoan(Loan loan);
	Loan updateLoan(Loan loan);
	void deleteLoan(Loan loan);
	void deleteLoan(Long id);

}
