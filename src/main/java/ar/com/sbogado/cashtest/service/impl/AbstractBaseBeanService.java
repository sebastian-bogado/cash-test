package ar.com.sbogado.cashtest.service.impl;

import ar.com.sbogado.cashtest.model.BaseBean;

import java.util.UUID;

public abstract class AbstractBaseBeanService<T extends BaseBean> extends AbstractTimestampService<T> {

	@Override
	protected T prepareToCreate(T baseBean) {
		baseBean.setUuid(UUID.randomUUID().toString());
		return super.prepareToCreate(baseBean);
	}

}
