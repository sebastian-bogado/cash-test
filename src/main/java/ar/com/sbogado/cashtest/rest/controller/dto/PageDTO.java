package ar.com.sbogado.cashtest.rest.controller.dto;

import lombok.Data;

import java.util.List;

@Data
public class PageDTO<T> {
	private List<T> items;
	private PagingDTO paging;
}
