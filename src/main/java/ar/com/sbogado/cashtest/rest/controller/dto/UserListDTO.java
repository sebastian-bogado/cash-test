package ar.com.sbogado.cashtest.rest.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class UserListDTO {

	private Long id;
	@NotNull
	@NotEmpty
	@Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",
			message = "{invalid.format.user.email}")
	@JsonProperty("email")
	private String email;
	@NotNull
	@NotEmpty
	@Pattern(regexp = "[A-Za-z]*?", message = "{invalid.format.user.firstName}")
	@JsonProperty("first_name")
	private String firstName;
	@NotNull
	@NotEmpty
	@Pattern(regexp = "[A-Za-z]*?", message = "{invalid.format.user.lastName}")
	@JsonProperty("last_name")
	private String lastName;

}
