package ar.com.sbogado.cashtest.rest.controller.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDTO extends UserListDTO {

	private List<LoanDTO> loans;

}
