package ar.com.sbogado.cashtest.rest.controller.mapper;

import ar.com.sbogado.cashtest.model.Loan;
import ar.com.sbogado.cashtest.rest.controller.dto.LoanDTO;
import ma.glasnost.orika.MapperFactory;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;
import org.springframework.stereotype.Component;

@Component
public class BussinessToDTOMapper implements OrikaMapperFactoryConfigurer {

	@Override
	public void configure(MapperFactory mapperFactory) {
		mapperFactory.classMap(Loan.class, LoanDTO.class)
				.field("id", "id")
				.field("user.id", "userId")
				.field("total", "total")
				.byDefault()
				.register();
	}

}
