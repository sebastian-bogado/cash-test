package ar.com.sbogado.cashtest.rest.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PageLoanFilterDTO {
	@NotNull
	@JsonProperty("limit")
	private Integer limit;
	@NotNull
	@JsonProperty("offset")
	private Integer offset;
	@JsonProperty("id")
	private Long userId;

}
