package ar.com.sbogado.cashtest.rest.controller;

import ar.com.sbogado.cashtest.dao.filter.LoanFilter;
import ar.com.sbogado.cashtest.model.Loan;
import ar.com.sbogado.cashtest.rest.controller.dto.LoanDTO;
import ar.com.sbogado.cashtest.rest.controller.dto.PageDTO;
import ar.com.sbogado.cashtest.rest.controller.dto.PagingDTO;
import ar.com.sbogado.cashtest.service.LoanService;
import ar.com.sbogado.cashtest.service.UserService;
import io.swagger.annotations.ApiOperation;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/loans")
public class LoanRestController {
	@Autowired
	private LoanService loanService;
	@Autowired
	private MapperFacade orikaMapper;

	@GetMapping("")
	@ApiOperation(value = "Get loans", notes = "Get loans",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public PageDTO<LoanDTO> getLoans(@RequestParam("offset") Integer offset,
																	 @RequestParam("limit") Integer limit,
																	 @RequestParam(name = "user_id", required = false) Long userId) {
		Pageable pageable = PageRequest.of(offset, limit);
		LoanFilter filter = new LoanFilter();
		filter.setUserId(userId);
		Page<Loan> result = loanService.findByFilter(filter, pageable);
		PageDTO<LoanDTO> pageDTO = new PageDTO<>();
		pageDTO.setItems(orikaMapper.mapAsList(result.getContent(), LoanDTO.class));
		PagingDTO pagingDTO = new PagingDTO();
		pagingDTO.setLimit(limit);
		pagingDTO.setOffset(offset);
		pagingDTO.setTotal(result.getTotalElements());
		pageDTO.setPaging(pagingDTO);
		return pageDTO;
	}


}
