package ar.com.sbogado.cashtest.rest.controller.dto;

import lombok.Data;

@Data
public class PagingDTO {

	private Integer offset;
	private Integer limit;
	private Long total;

}
