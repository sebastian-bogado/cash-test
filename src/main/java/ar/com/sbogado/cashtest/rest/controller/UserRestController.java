package ar.com.sbogado.cashtest.rest.controller;

import ar.com.sbogado.cashtest.exception.NotValidFormatException;
import ar.com.sbogado.cashtest.model.User;
import ar.com.sbogado.cashtest.rest.controller.dto.UserDTO;
import ar.com.sbogado.cashtest.rest.controller.dto.UserListDTO;
import ar.com.sbogado.cashtest.service.UserService;
import io.swagger.annotations.ApiOperation;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserRestController {

	@Autowired
	private UserService userService;
	@Autowired
	private MapperFacade orikaMapper;

	@GetMapping("")
	@ApiOperation(value = "Get list of users", notes = "Get list of users",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<UserListDTO> getUsers() {
		return orikaMapper.mapAsList(userService.readActiveUsers(), UserListDTO.class);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Get user by id", notes = "Get user by id",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO readUser(@NotNull @PathVariable("id") Long userId) {
		return orikaMapper.map(userService.readUserWithAllInfoById(userId), UserDTO.class);
	}

	@PostMapping
	@ApiOperation(value = "Create user", notes = "Create user",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO createUser(@RequestBody @Valid @NotNull UserDTO userDTO) {
		return orikaMapper.map(userService.createUser(orikaMapper.map(userDTO, User.class)), UserDTO.class);
	}

	@PutMapping
	@ApiOperation(value = "Update user", notes = "Update user",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO updateUser(@RequestBody @Valid @NotNull UserDTO userDTO) {
		if(userDTO.getId() == null) {
			throw new NotValidFormatException("user.id.isnot.present");
		}
		return orikaMapper.map(userService.updateUser(orikaMapper.map(userDTO, User.class)), UserDTO.class);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Delete user", notes = "Delete user",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public void deleteUser(@NotNull @PathVariable("id") Long id) {
		userService.deleteUser(id);
	}

}
