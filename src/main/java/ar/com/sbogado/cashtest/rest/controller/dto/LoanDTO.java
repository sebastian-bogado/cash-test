package ar.com.sbogado.cashtest.rest.controller.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class LoanDTO {

	private Long id;
	private Long userId;
	@NotNull
	private BigDecimal total;

}
