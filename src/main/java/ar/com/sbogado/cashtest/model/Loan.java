package ar.com.sbogado.cashtest.model;

import lombok.Data;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.SQLDelete;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Entity
@SQLDelete(sql = "update loan SET active = 0 WHERE id=?")
public class Loan extends LogicalDeleteableBean {

	@ManyToOne
	@Cascade(CascadeType.DETACH)
	private User user;
	@NotNull
	private BigDecimal total;

}
