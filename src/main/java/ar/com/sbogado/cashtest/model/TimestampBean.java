package ar.com.sbogado.cashtest.model;

import lombok.Data;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.util.Date;

@MappedSuperclass
@Data
public abstract class TimestampBean {
	@NotNull
	private Date createdFrom;
	@NotNull
	private Date lastUpdate;
}
