package ar.com.sbogado.cashtest.model;

import lombok.Data;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.SQLDelete;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Entity
@Data
@SQLDelete(sql = "update USER_INFO SET active = 0 WHERE id=?")
@Table(name = "USER_INFO")
public class User extends LogicalDeleteableBean {

	@NotNull
	@NotEmpty
	@Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
	private String email;
	@NotNull
	@NotEmpty
	@Pattern(regexp = "[A-Za-z]*?")
	private String firstName;
	@NotNull
	@NotEmpty
	@Pattern(regexp = "[A-Za-z]*?")
	private String lastName;
	@OneToMany(fetch = FetchType.LAZY)
	@Cascade(CascadeType.DELETE)
	private List<Loan> loans;

}