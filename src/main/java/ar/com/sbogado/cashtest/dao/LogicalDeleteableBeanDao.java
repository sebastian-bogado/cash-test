package ar.com.sbogado.cashtest.dao;

import ar.com.sbogado.cashtest.model.LogicalDeleteableBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface LogicalDeleteableBeanDao<T extends LogicalDeleteableBean> extends BaseBeanDao<T> {

	List<T> findAllByActiveIsTrue();
	Page<T> findAllByActiveIsTrue(Pageable pageable);
	List<T> findAllByActiveIsFalse();
	Page<T> findAllByActiveIsFalse(Pageable pageable);

	Optional<T> findByIdAndActiveIsTrue(Long id);

	Optional<T> findByIdAndActiveIsFalse(Long id);

}
