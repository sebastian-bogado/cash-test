package ar.com.sbogado.cashtest.dao;

import ar.com.sbogado.cashtest.dao.filter.LoanFilter;
import ar.com.sbogado.cashtest.model.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CustomLoanDao {

	Page<Loan> findLoansByFilterAndActiveIsTrue(LoanFilter filter, Pageable pageable);

}
