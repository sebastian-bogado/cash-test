package ar.com.sbogado.cashtest.dao;

import ar.com.sbogado.cashtest.model.TimestampBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Date;
import java.util.List;

@NoRepositoryBean
public interface TimestampBeanDao<T extends TimestampBean> extends JpaRepository<T, Long> {

	List<T> findByLastUpdateIsBefore(Date comparationDate);

	List<T> findByCreatedFromIsBefore(Date comparationDate);

	List<T> findByLastUpdateIsAfter(Date comparationDate);

	List<T> findByCreatedFromIsAfter(Date comparationDate);

}
