package ar.com.sbogado.cashtest.dao;

import ar.com.sbogado.cashtest.model.User;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface UserDao extends LogicalDeleteableBeanDao<User> {

	Optional<User> findByEmail(@NotNull @NotEmpty String email);
}
