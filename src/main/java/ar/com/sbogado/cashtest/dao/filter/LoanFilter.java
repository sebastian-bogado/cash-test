package ar.com.sbogado.cashtest.dao.filter;

import lombok.Data;

@Data
public class LoanFilter {
	private Long userId;
}
