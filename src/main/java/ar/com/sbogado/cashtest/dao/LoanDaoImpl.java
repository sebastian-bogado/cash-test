package ar.com.sbogado.cashtest.dao;

import ar.com.sbogado.cashtest.dao.filter.LoanFilter;
import ar.com.sbogado.cashtest.model.Loan;
import ar.com.sbogado.cashtest.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class LoanDaoImpl implements CustomLoanDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Page<Loan> findLoansByFilterAndActiveIsTrue(LoanFilter filter, Pageable pageable) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Loan> query = cb.createQuery(Loan.class);
		Root<Loan> loan = query.from(Loan.class);
		CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
		Root<Loan> countRoot = countQuery.from(Loan.class);
		countQuery.select(cb.countDistinct(countRoot));

		query.distinct(Boolean.TRUE);
		query.orderBy(cb.desc(loan.get("createdFrom")));
		List<Predicate> predicateList = new ArrayList<>();
		List<Predicate> predicateCountList = new ArrayList<>();
		if (filter != null) {
			if (filter.getUserId() != null) {
				Join<Loan, User> user = loan.join("user", JoinType.INNER);
				Join<Loan, User> countUser = countRoot.join("user", JoinType.INNER);
				predicateList.add(cb.equal(user.get("id"), filter.getUserId()));
				predicateCountList.add(cb.equal(countUser.get("id"), filter.getUserId()));
			}
		}
		predicateList.add(cb.isTrue(loan.get("active")));


		query.select(loan).where(predicateList.toArray(new Predicate[predicateList.size()]));
		entityManager.createQuery(query);
		TypedQuery typedQuery = entityManager.createQuery(query);
		typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		typedQuery.setMaxResults(pageable.getPageSize());
		List<Loan> resultList = typedQuery.getResultList();

		predicateCountList.addAll(predicateList);
		entityManager.createQuery(countQuery);
		countQuery.where(predicateCountList.toArray(new Predicate[predicateList.size()]));
		Long count = entityManager.createQuery(countQuery).getSingleResult();

		return new PageImpl<>(resultList, pageable, count);

	}

}
