package ar.com.sbogado.cashtest.dao;

import ar.com.sbogado.cashtest.model.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanDao extends LogicalDeleteableBeanDao<Loan>, CustomLoanDao {
	Page<Loan> findAllByActiveIsTrue(Pageable pageable);


}
