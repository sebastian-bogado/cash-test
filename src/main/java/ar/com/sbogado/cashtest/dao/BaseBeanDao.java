package ar.com.sbogado.cashtest.dao;

import ar.com.sbogado.cashtest.model.BaseBean;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface BaseBeanDao<T extends BaseBean> extends TimestampBeanDao<T> {

	Optional<T> findById(Long id);
	Optional<T> findByUuid(String uuid);

}
