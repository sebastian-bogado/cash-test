package ar.com.sbogado.cashtest.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class NotValidFormatException extends BusinessException {

	public NotValidFormatException(String descriptionMessage) {
		super(4009, "not.valid.format.exception.message", null, descriptionMessage, null, HttpStatus.BAD_REQUEST);
	}

}
