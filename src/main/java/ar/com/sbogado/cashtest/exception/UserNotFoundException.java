package ar.com.sbogado.cashtest.exception;

import org.springframework.http.HttpStatus;

public class UserNotFoundException extends BusinessException {
	public UserNotFoundException(Long id) {
		super(4041,"user.not.found.exception.message", null, "user.not.found.exception.description", new String[]{id.toString()}, HttpStatus.NOT_FOUND);
	}
}
