package ar.com.sbogado.cashtest.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomError {

	private Integer code;
	private String message;
	private String description;
	private Integer status;

}
