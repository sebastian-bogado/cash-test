package ar.com.sbogado.cashtest.exception;

import org.springframework.http.HttpStatus;

public class LoanNotFoundException extends BusinessException {
	public LoanNotFoundException(Long id) {
		super(4041,"loan.not.found", new String[]{id.toString()}, "loan.not.found.description", null, HttpStatus.NOT_FOUND);
	}

	public LoanNotFoundException(String messageCode) {
		super(4041,"loan.not.found", null, messageCode, null, HttpStatus.NOT_FOUND);
	}

}
