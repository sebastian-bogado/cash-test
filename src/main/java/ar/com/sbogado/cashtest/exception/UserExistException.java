package ar.com.sbogado.cashtest.exception;

import org.springframework.http.HttpStatus;

public class UserExistException extends BusinessException {

	public UserExistException(String email) {
		super(999, "user.exist.exception.message",new String[]{email}, "user.exist.exception.description",null, HttpStatus.CONFLICT);
	}
}
