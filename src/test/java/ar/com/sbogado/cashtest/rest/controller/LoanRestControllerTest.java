package ar.com.sbogado.cashtest.rest.controller;

import ar.com.sbogado.cashtest.model.Loan;
import ar.com.sbogado.cashtest.model.User;
import ar.com.sbogado.cashtest.rest.controller.dto.PageDTO;
import ar.com.sbogado.cashtest.service.LoanService;
import ar.com.sbogado.cashtest.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LoanRestControllerTest {

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Autowired
	private LoanService loanService;
	@Autowired
	private UserService userService;

	private User user;

	@Before
	public void initialize() {
		user = new User();
		user.setLastName("Bogado");
		user.setFirstName("Sebastian");
		user.setEmail("test5@loan.com");
		user.setLastUpdate(new Date());
		user.setCreatedFrom(new Date());
		user.setUuid("25");
		user.setActive(true);
		user = userService.createUser(user);
		User user2 = new User();
		user2.setLastName("Bogado");
		user2.setFirstName("Sebastian");
		user2.setEmail("test6@loan.com");
		user2.setLastUpdate(new Date());
		user2.setCreatedFrom(new Date());
		user2.setUuid("26");
		user2.setActive(true);
		user2 = userService.createUser(user2);
		Loan loan = new Loan();
		loan.setTotal(BigDecimal.valueOf(1230944.56));
		loan.setUser(user);
		loan.setActive(true);
		loan.setLastUpdate(new Date());
		loan.setCreatedFrom(new Date());
		loan.setUuid("25");
		loan = loanService.createLoan(loan);
		loan.getUser();
		Loan loan2 = new Loan();
		loan2.setTotal(BigDecimal.valueOf(1230944.56));
		loan2.setUser(user2);
		loan2.setActive(true);
		loan2.setLastUpdate(new Date());
		loan2.setCreatedFrom(new Date());
		loan2.setUuid("26");
		loanService.createLoan(loan2);
	}


	@Test
	public void findLoans() {
		PageDTO response = testRestTemplate.getForObject("/loans?offset=0&limit=2", PageDTO.class);
		Assert.assertEquals(2, response.getItems().size());
	}
	@Test
	public void findLoansByUser() {
		PageDTO response = testRestTemplate.getForObject("/loans?offset=0&limit=2&user_id="+user.getId().toString(), PageDTO.class);
		Assert.assertEquals(1, response.getItems().size());
	}


}
