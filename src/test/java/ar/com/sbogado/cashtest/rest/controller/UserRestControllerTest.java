package ar.com.sbogado.cashtest.rest.controller;

import ar.com.sbogado.cashtest.rest.controller.dto.UserDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserRestControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void createUser() {
		UserDTO userDTO = new UserDTO();
		userDTO.setEmail("seebogado@gmail.com");
		userDTO.setFirstName("UserUno");
		userDTO.setLastName("LastnameUno");
		UserDTO response = restTemplate.postForObject("/users", userDTO, UserDTO.class);
		Assert.assertNotNull(response.getId());
	}


	@Test
	public void findUserById() {
		UserDTO userDTO = new UserDTO();
		userDTO.setEmail("rest.user1@test.com");
		userDTO.setFirstName("UserDOs");
		userDTO.setLastName("LastnameDos");
		UserDTO response = restTemplate.postForObject("/users", userDTO, UserDTO.class);
		UserDTO user = restTemplate.getForObject("/users/" + response.getId(), UserDTO.class);
		Assert.assertNotNull(user.getId());
	}

	@Test
	public void deleteUser() {
		UserDTO userDTO = new UserDTO();
		userDTO.setEmail("rest.user3@test.com");
		userDTO.setFirstName("UserTres");
		userDTO.setLastName("LastnameTres");
		UserDTO response = restTemplate.postForObject("/users", userDTO, UserDTO.class);
		UserDTO user = restTemplate.getForObject("/users/" + response.getId(), UserDTO.class);
		Assert.assertNotNull(user.getId());
		restTemplate.delete("/users/" + user.getId());
		ResponseEntity<UserDTO> userResponse = restTemplate.getForEntity("/users/" + user.getId(), UserDTO.class);
		Assert.assertEquals(404, userResponse.getStatusCodeValue());
	}


}
