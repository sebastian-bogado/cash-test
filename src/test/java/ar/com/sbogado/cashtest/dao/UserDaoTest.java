package ar.com.sbogado.cashtest.dao;

import ar.com.sbogado.cashtest.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDaoTest {

	@Autowired
	private UserDao userDao;

	@Test
	public void createUser() {
		User user = new User();
		user.setLastName("Bogado");
		user.setFirstName("Sebastian");
		user.setEmail("enrique.bogado@ducode.io");
		user.setLastUpdate(new Date());
		user.setCreatedFrom(new Date());
		user.setUuid(UUID.randomUUID().toString());
		user.setActive(true);
		user.setLoans(new ArrayList<>());
		user = userDao.save(user);
		Assert.assertNotNull(user.getId());
	}

	@Test
	public void updateUser() {
		User user = new User();
		user.setLastName("Bogado");
		user.setFirstName("Sebastian");
		user.setEmail("sebastian.bogado@yopmail.com");
		user.setLastUpdate(new Date());
		user.setCreatedFrom(new Date());
		user.setUuid(UUID.randomUUID().toString());
		user.setActive(true);
		user.setLoans(new ArrayList<>());
		user = userDao.save(user);
		user.setEmail("prueba2@prueba2.com.ar");
		userDao.save(user);
	}

	@Test
	public void findUser() {
		User user = new User();
		user.setLastName("Ocampo");
		user.setFirstName("Emanuel");
		user.setEmail("emanuel.ocampo@yopmail.com");
		user.setLastUpdate(new Date());
		user.setCreatedFrom(new Date());
		user.setUuid(UUID.randomUUID().toString());
		user.setActive(true);
		user = userDao.save(user);
		Assert.assertTrue(userDao.findById(user.getId()).isPresent());
	}

	@Test
	public void deleteUser() {
		User user = new User();
		user.setLastName("Bogado");
		user.setFirstName("Sebastian");
		user.setEmail("emanuel.bogado@yopmail.com");
		user.setLastUpdate(new Date());
		user.setCreatedFrom(new Date());
		user.setUuid(UUID.randomUUID().toString());
		user.setActive(true);
		user.setLoans(new ArrayList<>());
		user = userDao.save(user);
		userDao.deleteById(user.getId());
		Optional<User> optUser = userDao.findByIdAndActiveIsTrue(user.getId());
		Assert.assertFalse(optUser.isPresent());
	}

	@Test
	public void findUsers() {
		User user1 = new User();
		user1.setLastName("Ocampo");
		user1.setFirstName("Jorge");
		user1.setEmail("jorge.ocampo@yopmail.com");
		user1.setLastUpdate(new Date());
		user1.setCreatedFrom(new Date());
		user1.setUuid(UUID.randomUUID().toString());
		user1.setActive(true);
		User user2 = new User();
		user2.setLastName("Ocampo");
		user2.setFirstName("Jorge");
		user2.setEmail("jorge.ocampo2@yopmail.com");
		user2.setLastUpdate(new Date());
		user2.setCreatedFrom(new Date());
		user2.setUuid(UUID.randomUUID().toString());
		user2.setActive(true);
		userDao.saveAll(Arrays.asList(user1, user2));
		Assert.assertTrue(userDao.findAllByActiveIsTrue().size() >= 2);
	}


}
