package ar.com.sbogado.cashtest.dao;

import ar.com.sbogado.cashtest.model.Loan;
import ar.com.sbogado.cashtest.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoanDaoTest {
	@Autowired
	private LoanDao loanDao;
	@Autowired
	private UserDao userDao;

	private User user;
	private static int cont= 0;


	@Before
	public void initialize() {
		cont++;
		user = new User();
		user.setLastName("Bogado");
		user.setFirstName("Sebastian");
		user.setEmail("test"+Integer.toString(cont)+"@loan.com");
		user.setLastUpdate(new Date());
		user.setCreatedFrom(new Date());
		user.setUuid(Integer.toString(cont));
		user.setActive(true);
		user = userDao.save(user);
	}


	@Test
	public void creaeteLoan() {

		Loan loan = new Loan();
		loan.setTotal(BigDecimal.valueOf(1230944.56));
		loan.setUser(user);
		loan.setActive(true);
		loan.setLastUpdate(new Date());
		loan.setCreatedFrom(new Date());
		loan.setUuid("1");
		loan = loanDao.save(loan);
		Assert.assertNotNull(loan.getId());
	}

	@Test
	public void updateLoan() {
		Loan loan = new Loan();
		loan.setTotal(BigDecimal.valueOf(1230944.56));
		loan.setUser(user);
		loan.setActive(true);
		loan.setLastUpdate(new Date());
		loan.setCreatedFrom(new Date());
		loan.setUuid("2");
		loan = loanDao.save(loan);
		loan.setTotal(BigDecimal.valueOf(123));
		loan = loanDao.save(loan);
		Assert.assertEquals(123,loan.getTotal().intValue());
	}

	@Test
	public void findLoan() {
		Loan loan = new Loan();
		loan.setTotal(BigDecimal.valueOf(1230944.56));
		loan.setUser(user);
		loan.setActive(true);
		loan.setLastUpdate(new Date());
		loan.setCreatedFrom(new Date());
		loan.setUuid("3");
		loan = loanDao.save(loan);
		Assert.assertTrue(loanDao.findById(loan.getId()).isPresent());
	}

	@Test
	public void deleteLoan() {
		Loan loan = new Loan();
		loan.setTotal(BigDecimal.valueOf(1230944.56));
		loan.setUser(user);
		loan.setActive(true);
		loan.setLastUpdate(new Date());
		loan.setCreatedFrom(new Date());
		loan.setUuid("4");
		loan = loanDao.save(loan);
		loanDao.deleteById(loan.getId());
		Assert.assertFalse(loanDao.findByIdAndActiveIsTrue(loan.getId()).isPresent());
	}

	@Test
	public void findLoans() {
		Loan loan = new Loan();
		loan.setTotal(BigDecimal.valueOf(1230944.56));
		loan.setUser(user);
		loan.setActive(true);
		loan.setLastUpdate(new Date());
		loan.setCreatedFrom(new Date());
		loan.setUuid("5");
		loan = loanDao.save(loan);
		loan.getUser();
		Loan loan2 = new Loan();
		loan2.setTotal(BigDecimal.valueOf(1230944.56));
		loan2.setUser(user);
		loan2.setActive(true);
		loan2.setLastUpdate(new Date());
		loan2.setCreatedFrom(new Date());
		loan2.setUuid("6");
		loanDao.save(loan2);
		Assert.assertTrue(loanDao.findAllByActiveIsTrue().size() >= 2);
	}

}
