# cash-test

Antes de compilar con maven este proyecto se debe hacer una de las dos cosas.

* En el primer caso, debe comentarse la dependencia de posgreSQL y descomentar la dependencia de h2 
  del archivo pom.xml
* En el segundo caso, se deben configurar las properties de la base de datos PosgreSQL, para ello se debe utilizar algo como:
    
  spring.datasource.driverClassName=org.postgresql.Driver
  
  spring.jpa.database-platform=org.hibernate.dialect.PostgreSQLDialect
  
  spring.datasource.platform=postgres
  
  spring.datasource.url=jdbc:postgresql://localhost:5432/testdb
  
  spring.datasource.username=user12
  
  spring.datasource.password=s$cret
    
Una vez hecho esto, con maven 3 realizar un clean install. La aplicacion deberia correr perfectamente ensu ambiente.
cualquier duda 
seebogado@gmail.com

* Descripcion de las apis: https://cash-test.herokuapp.com/swagger-ui.html
* Url base de las apis: https://cash-test.herokuapp.com
